# $HOME

This is the `$HOME` for my NixOS setup, using the `unstable` channel.

## Useful Commands

- To `ping` but with a graph: `$ gping`
  - `$ gping 1.1.1.1`
