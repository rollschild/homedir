--[[
--This is the Lua config file for Neovim, on NixOS
--]]
-- this should be at the very start of this file
-- disable netrw
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.g.mapleader = " "
-- markdown-preview.nvim
vim.g.mkdp_auto_start = 0
-- vim-markdown
vim.g.vim_markdown_conceal = 0
vim.g.vim_markdown_conceal_code_blocks = 0

-- map gx to open link under cursor
-- https://stackoverflow.com/a/63352904/9666932
vim.keymap.set("n", "gx", ":!xdg-open <c-r><c-a><CR>")

-- Colorscheme
vim.o.background = "dark" -- or "light" for light mode
-- Default options:
require("gruvbox").setup({
	bold = true,
	italic = {
		strings = true,
		emphasis = true,
		comments = true,
		operators = false,
		folds = true,
	},
	strikethrough = true,
	invert_selection = false,
	invert_signs = false,
	invert_tabline = false,
	invert_intend_guides = false,
	inverse = true, -- invert background for search, diffs, statuslines and errors
	contrast = "hard", -- can be "hard", "soft" or empty string
	palette_overrides = {},
	overrides = {},
})
vim.cmd([[colorscheme gruvbox]])

-- Mason
local mason_config = {}
local mason_lspconfig_config = {
	ensure_installed = {
		"ts_ls",
		"pyright",
		"clangd",
		"rust_analyzer",
		"lua_ls",
		"marksman",
		"cmake",
		"erlangls",
		"nil_ls",
		"eslint",
		"bashls",
		"ruff",
	},
	automatic_installation = true,
}
require("mason").setup(mason_config)
require("mason-lspconfig").setup(mason_lspconfig_config)

-- nvim-tree
local nvim_tree_lib = require("nvim-tree.lib")
local openfile = require("nvim-tree.actions.node.open-file")
local actions = require("telescope.actions")
local action_state = require("telescope.actions.state")
local M = {}

local view_selection = function(prompt_bufnr, map)
	actions.select_default:replace(function()
		actions.close(prompt_bufnr)
		local selection = action_state.get_selected_entry()
		local filename = selection.filename
		if filename == nil then
			filename = selection[1]
		end
		openfile.fn("preview", filename)
	end)
	return true
end

function M.launch_live_grep(opts)
	return M.launch_telescope("live_grep", opts)
end

function M.launch_find_files(opts)
	return M.launch_telescope("find_files", opts)
end

function M.launch_telescope(func_name, opts)
	local telescope_status_ok, _ = pcall(require, "telescope")
	if not telescope_status_ok then
		return
	end
	local lib_status_ok, lib = pcall(require, "nvim-tree.lib")
	if not lib_status_ok then
		return
	end
	local node = lib.get_node_at_cursor()
	local is_folder = node.fs_stat and node.fs_stat.type == "directory" or false
	local basedir = is_folder and node.absolute_path or vim.fn.fnamemodify(node.absolute_path, ":h")
	if node.name == ".." and TreeExplorer ~= nil then
		basedir = TreeExplorer.cwd
	end
	opts = opts or {}
	opts.cwd = basedir
	opts.search_dirs = { basedir }
	opts.attach_mappings = view_selection
	return require("telescope.builtin")[func_name](opts)
end

local function custom_callback(callback_name)
	return string.format(":lua require('treeutils').%s()<CR>", callback_name)
end

local function nvim_tree_on_attach(bufnr)
	local api = require("nvim-tree.api")

	local function opts(desc)
		return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
	end

	-- Default mappings.
	-- BEGIN_DEFAULT_ON_ATTACH
	vim.keymap.set("n", "<C-]>", api.tree.change_root_to_node, opts("CD"))
	vim.keymap.set("n", "<C-e>", api.node.open.replace_tree_buffer, opts("Open: In Place"))
	vim.keymap.set("n", "<C-k>", api.node.show_info_popup, opts("Info"))
	vim.keymap.set("n", "<C-r>", api.fs.rename_sub, opts("Rename: Omit Filename"))
	vim.keymap.set("n", "<C-t>", api.node.open.tab, opts("Open: New Tab"))
	vim.keymap.set("n", "<C-v>", api.node.open.vertical, opts("Open: Vertical Split"))
	vim.keymap.set("n", "<C-x>", api.node.open.horizontal, opts("Open: Horizontal Split"))
	vim.keymap.set("n", "<BS>", api.node.navigate.parent_close, opts("Close Directory"))
	vim.keymap.set("n", "<CR>", api.node.open.edit, opts("Open"))
	vim.keymap.set("n", "<Tab>", api.node.open.preview, opts("Open Preview"))
	vim.keymap.set("n", ">", api.node.navigate.sibling.next, opts("Next Sibling"))
	vim.keymap.set("n", "<", api.node.navigate.sibling.prev, opts("Previous Sibling"))
	vim.keymap.set("n", ".", api.node.run.cmd, opts("Run Command"))
	vim.keymap.set("n", "-", api.tree.change_root_to_parent, opts("Up"))
	vim.keymap.set("n", "a", api.fs.create, opts("Create"))
	vim.keymap.set("n", "bmv", api.marks.bulk.move, opts("Move Bookmarked"))
	vim.keymap.set("n", "B", api.tree.toggle_no_buffer_filter, opts("Toggle No Buffer"))
	vim.keymap.set("n", "c", api.fs.copy.node, opts("Copy"))
	vim.keymap.set("n", "C", api.tree.toggle_git_clean_filter, opts("Toggle Git Clean"))
	vim.keymap.set("n", "[c", api.node.navigate.git.prev, opts("Prev Git"))
	vim.keymap.set("n", "]c", api.node.navigate.git.next, opts("Next Git"))
	vim.keymap.set("n", "d", api.fs.remove, opts("Delete"))
	vim.keymap.set("n", "D", api.fs.trash, opts("Trash"))
	vim.keymap.set("n", "E", api.tree.expand_all, opts("Expand All"))
	vim.keymap.set("n", "e", api.fs.rename_basename, opts("Rename: Basename"))
	vim.keymap.set("n", "]e", api.node.navigate.diagnostics.next, opts("Next Diagnostic"))
	vim.keymap.set("n", "[e", api.node.navigate.diagnostics.prev, opts("Prev Diagnostic"))
	vim.keymap.set("n", "F", api.live_filter.clear, opts("Clean Filter"))
	vim.keymap.set("n", "f", api.live_filter.start, opts("Filter"))
	vim.keymap.set("n", "g?", api.tree.toggle_help, opts("Help"))
	vim.keymap.set("n", "gy", api.fs.copy.absolute_path, opts("Copy Absolute Path"))
	vim.keymap.set("n", "H", api.tree.toggle_hidden_filter, opts("Toggle Dotfiles"))
	vim.keymap.set("n", "I", api.tree.toggle_gitignore_filter, opts("Toggle Git Ignore"))
	vim.keymap.set("n", "J", api.node.navigate.sibling.last, opts("Last Sibling"))
	vim.keymap.set("n", "K", api.node.navigate.sibling.first, opts("First Sibling"))
	vim.keymap.set("n", "m", api.marks.toggle, opts("Toggle Bookmark"))
	vim.keymap.set("n", "o", api.node.open.edit, opts("Open"))
	vim.keymap.set("n", "O", api.node.open.no_window_picker, opts("Open: No Window Picker"))
	vim.keymap.set("n", "p", api.fs.paste, opts("Paste"))
	vim.keymap.set("n", "P", api.node.navigate.parent, opts("Parent Directory"))
	vim.keymap.set("n", "q", api.tree.close, opts("Close"))
	vim.keymap.set("n", "r", api.fs.rename, opts("Rename"))
	vim.keymap.set("n", "R", api.tree.reload, opts("Refresh"))
	vim.keymap.set("n", "s", api.node.run.system, opts("Run System"))
	vim.keymap.set("n", "S", api.tree.search_node, opts("Search"))
	vim.keymap.set("n", "U", api.tree.toggle_custom_filter, opts("Toggle Hidden"))
	vim.keymap.set("n", "W", api.tree.collapse_all, opts("Collapse"))
	vim.keymap.set("n", "x", api.fs.cut, opts("Cut"))
	vim.keymap.set("n", "y", api.fs.copy.filename, opts("Copy Name"))
	vim.keymap.set("n", "Y", api.fs.copy.relative_path, opts("Copy Relative Path"))
	vim.keymap.set("n", "<2-LeftMouse>", api.node.open.edit, opts("Open"))
	vim.keymap.set("n", "<2-RightMouse>", api.tree.change_root_to_node, opts("CD"))
	-- END_DEFAULT_ON_ATTACH

	-- Mappings removed via:
	vim.keymap.set("n", "l", "", { buffer = bufnr })
	vim.keymap.del("n", "l", { buffer = bufnr })
	vim.keymap.set("n", "h", "", { buffer = bufnr })
	vim.keymap.del("n", "h", { buffer = bufnr })

	-- Mappings migrated from view.mappings.list
	-- Custom key mappings: with a custom action_cb
	vim.keymap.set("n", "s", api.node.open.horizontal, opts("Open: Horizontal Split"))
	vim.keymap.set("n", "v", api.node.open.vertical, opts("Open: Vertical Split"))
end

local nvim_tree_config = {
	view = {
		side = "left",
		width = 32,
	},
	filters = {
		dotfiles = false,
	},
	git = {
		enable = true,
		ignore = false,
		show_on_dirs = true,
		show_on_open_dirs = true,
	},
	-- always expand and focus the corresponding directory whenever a file is opened in the current buffer
	update_cwd = true,
	update_focused_file = {
		enable = true,
		update_cwd = true,
		-- temp fix due to: https://github.com/nvim-tree/nvim-tree.lua/issues/2057#issuecomment-1472951544
		ignore_list = { "toggleterm", "term" },
	},
	on_attach = nvim_tree_on_attach,
}
require("nvim-tree").setup(nvim_tree_config)
local function open_nvim_tree(data)
	-- buffer is a real file on the disk
	local real_file = vim.fn.filereadable(data.file) == 1
	if real_file then
		-- require("nvim-tree.api").tree.toggle({ focus = false, find_file = true })
		return
	end

	-- buffer is a [No Name]
	local no_name = data.file == "" and vim.bo[data.buf].buftype == ""

	-- buffer is a directory
	local directory = vim.fn.isdirectory(data.file) == 1

	if not no_name and not directory then
		return
	end

	--[[
	if real_file then
		-- require("nvim-tree.api").tree.toggle({ focus = true, find_file = true })
		return
	end
	--]]
	-- change to the directory
	if directory then
		-- create a new, empty buffer
		vim.cmd.enew()

		-- wipe the directory buffer
		vim.cmd.bw(data.buf)

		-- change to the directory
		vim.cmd.cd(data.file)

		-- open the tree
		require("nvim-tree.api").tree.open()
		return
	end

	-- open the tree and focus on the tree itself
	require("nvim-tree.api").tree.toggle({ focus = true, find_file = true })
end

vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_nvim_tree })

-- telescope
local builtin = require("telescope.builtin")
local telescope_actions = require("telescope.actions")
local default_telescope_config = require("telescope.config")
local vimgrep_arguments = { unpack(default_telescope_config.values.vimgrep_arguments) }
-- I want to search in hidden/dot files.
table.insert(vimgrep_arguments, "--hidden")
-- I don't want to search in the `.git` directory.
table.insert(vimgrep_arguments, "--glob")
table.insert(vimgrep_arguments, "!**/.git/*")
vim.keymap.set("n", "<C-f>", builtin.find_files, {})
vim.keymap.set("n", "<C-g>", builtin.live_grep, {})
vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})
local telescope_config = {
	defaults = {
		vimgrep_arguments = vimgrep_arguments,
		mappings = {
			i = {
				["<C-s>"] = telescope_actions.select_horizontal,
			},
		},
	},
	pickers = {
		find_files = {
			-- `hidden = true` will still show the inside of `.git/` as it's not `.gitignore`d.
			find_command = { "rg", "--files", "--hidden", "--glob", "!**/.git/*" },
		},
	},
	extensions = {
		fzf = {
			fuzzy = true, -- false will only do exact matching
			override_generic_sorter = true, -- override the generic sorter
			override_file_sorter = true, -- override the file sorter
			case_mode = "smart_case", -- or "ignore_case" or "respect_case"
			-- the default case_mode is "smart_case"
		},
	},
}
require("telescope").setup(telescope_config)
require("telescope").load_extension("fzf")

-- Comment.nvim
require("Comment").setup()
-- map <leader>/ under visual mode to toggle linewise comment
vim.keymap.set("v", "<leader>/", ":normal gcc<CR>")

-- toggleterm
-- Usage:
-- :ToggleTerm size=40 dir=~/Desktop direction=horizontal
local toggleterm_config = {
	-- size can be a number or function which is passed the current terminal
	size = function(term)
		if term.direction == "horizontal" then
			return 15
		elseif term.direction == "vertical" then
			return vim.o.columns * 0.25
		end
	end,
	open_mapping = "<c-t>",
	direction = "vertical",
	hide_numbers = true, -- hide the number column in toggleterm buffers
	shade_filetypes = {},
	autochdir = false, -- when neovim changes it current directory the terminal will change it's own when next it's opened
	shade_terminals = true, -- NOTE: this option takes priority over highlights specified so if you specify Normal highlights you should set this to false
	start_in_insert = true,
	insert_mappings = true, -- whether or not the open mapping applies in insert mode
	terminal_mappings = true, -- whether or not the open mapping applies in the opened terminals
	persist_size = true,
	persist_mode = true, -- if set to true (default) the previous terminal mode will be remembered
	close_on_exit = true, -- close the terminal window when the process exits
	shell = vim.o.shell, -- change the default shell
	auto_scroll = true, -- automatically scroll to the bottom on terminal output
}
require("toggleterm").setup(toggleterm_config)

-- indent-blankline
local indent_blankline_config = {
	indent = {
		char = "│",
		tab_char = "│",
	},
}
require("ibl").setup(indent_blankline_config)

--gitsigns
local gitsigns_config = {
	signs = {
		add = { text = "│" },
		change = { text = "│" },
		delete = { text = "_" },
		topdelete = { text = "‾" },
		changedelete = { text = "~" },
		untracked = { text = "┆" },
	},
	signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
	numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
	linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
	word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
	watch_gitdir = {
		interval = 1000,
		follow_files = true,
	},
	attach_to_untracked = true,
	current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
	current_line_blame_opts = {
		virt_text = true,
		virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
		delay = 1000,
		ignore_whitespace = false,
	},
	current_line_blame_formatter = "<author>, <author_time:%Y-%m-%d> - <summary>",
	sign_priority = 6,
	update_debounce = 100,
	status_formatter = nil, -- Use default
	max_file_length = 40000, -- Disable if file is longer than this (in lines)
	preview_config = {
		-- Options passed to nvim_open_win
		border = "single",
		style = "minimal",
		relative = "cursor",
		row = 0,
		col = 1,
	},
}
require("gitsigns").setup(gitsigns_config)

-- lualine
local lualine_config = {
	options = {
		theme = "gruvbox",
	},
}
require("lualine").setup(lualine_config)

-- bufferline
require("bufferline").setup({})

-- neodev
-- IMPORTANT: make sure to setup neodev BEFORE lspconfig
require("neodev").setup({})

-- nvim-lastplace
require("nvim-lastplace").setup({})

-- Autocompletion
-- Add additional capabilities supported by nvim-cmp
local cmp_nvim_lsp_capabilities = require("cmp_nvim_lsp").default_capabilities()
local client_capabilities = vim.lsp.protocol.make_client_capabilities()
local default_capabilities = client_capabilities
for k, v in pairs(cmp_nvim_lsp_capabilities) do
	default_capabilities[k] = v
end
local clangd_capabilities = default_capabilities
clangd_capabilities.offsetEncoding = { "utf-16" }

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
--[[ local servers = mason_lspconfig_config.ensure_installed
local exclusion_servers = {
	lua_ls = true,
}
for _, lsp in ipairs(servers) do
	if not exclusion_servers[lsp] then
		if lsp == "clangd" then
			lspconfig[lsp].setup({
				capabilities = clangd_capabilities,
			})
		else
			lspconfig[lsp].setup({
				capabilities = nvim_cmp_capabilities,
			})
		end
	end
end ]]
-- luasnip setup
local luasnip = require("luasnip")
-- nvim-cmp setup
local cmp = require("cmp")

luasnip.setup({ region_check_events = "InsertEnter", delete_check_events = "InsertEnter" })
require("luasnip.loaders.from_vscode").lazy_load()

local has_words_before = function()
	local line, col = unpack(vim.api.nvim_win_get_cursor(0))
	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

cmp.setup({
	preselect = cmp.PreselectMode.Item,
	sorting = {
		comparators = {
			-- The built-in comparators:
			cmp.config.compare.offset,
			cmp.config.compare.exact,
			require("clangd_extensions.cmp_scores"),
			cmp.config.compare.score,
			require("cmp-under-comparator").under,
			cmp.config.compare.kind,
			cmp.config.compare.sort_text,
			cmp.config.compare.length,
			cmp.config.compare.order,
		},
	},
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		-- ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
		["<C-u>"] = cmp.mapping.scroll_docs(-4), -- Up
		["<C-d>"] = cmp.mapping.scroll_docs(4), -- Down
		["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
		["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
		["<C-Space>"] = cmp.mapping.complete(),
		["<CR>"] = cmp.mapping.confirm({
			behavior = cmp.ConfirmBehavior.Replace,
			select = true,
		}),
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}),
	sources = {
		{ name = "nvim_lsp_signature_help" },
		{ name = "nvim_lsp" },
		{ name = "luasnip" },
		{ name = "nvim_lua" },
		{ name = "path" },
		-- { name = "buffer" },
	},
})

cmp.setup.cmdline("/", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = { { name = "nvim_lsp_document_symbol" }, { name = "buffer" } },
})

cmp.setup.cmdline(":", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({ { name = "path" } }, { { name = "cmdline" } }),
})

-- Lsp setup
-- https://github.com/neovim/nvim-lspconfig
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local lspconfig = require("lspconfig")
local root_pattern = lspconfig.util.root_pattern
local opts = { noremap = true, silent = true }
-- You will likely want to reduce updatetime which affects CursorHold
-- note: this setting is global and should be set only once
vim.cmd([[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false, scope="cursor"})]])

-- diagnostic config
vim.diagnostic.config({
	virtual_text = false, -- disable trailing virtual diagnostic text after the real code
	signs = true,
	underline = true,
	update_in_insert = true,
	severity_sort = false,
})

vim.keymap.set("n", "<leader>k", vim.diagnostic.open_float, opts)
vim.keymap.set("n", "<leader>e", ":NvimTreeToggle<CR>")
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
vim.keymap.set("n", "<space>q", vim.diagnostic.setloclist, opts)

-- Below on_attach is no longer recommended: https://github.com/neovim/nvim-lspconfig/pull/2514
-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

	-- Mappings.
	-- See `:help vim.lsp.*` for documentation on any of the below functions
	local bufopts = { noremap = true, silent = true, buffer = bufnr }
	vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
	vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
	vim.keymap.set("n", "S", vim.lsp.buf.hover, bufopts)
	vim.keymap.set("n", "gi", vim.lsp.buf.implementation, bufopts)
	vim.keymap.set("n", "<C-s>", vim.lsp.buf.signature_help, bufopts)
	vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, bufopts)
	vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
	vim.keymap.set("n", "<space>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, bufopts)
	vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, bufopts)
	vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, bufopts)
	vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, bufopts)
	vim.keymap.set("n", "gr", vim.lsp.buf.references, bufopts)
	vim.keymap.set("n", "<space>f", function()
		vim.lsp.buf.format({ async = true })
	end, bufopts)

	if client.supports_method("textDocument/formatting") then
		vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
		vim.api.nvim_create_autocmd("BufWritePre", {
			group = augroup,
			buffer = bufnr,
			callback = function()
				vim.lsp.buf.format({ bufnr = bufnr })
			end,
		})
	end
end
-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd("LspAttach", {
	group = vim.api.nvim_create_augroup("UserLspConfig", {}),
	callback = function(args)
		--[[
		local client = vim.lsp.get_client_by_id(args.data.client_id)
		if client ~= nil and client.name == "ruff" then
			-- Disable hover in favor of Pyright
			client.server_capabilities.hoverProvider = false
		end
		--]]
		-- Enable completion triggered by <c-x><c-o>
		vim.bo[args.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

		-- Buffer local mappings.
		-- See `:help vim.lsp.*` for documentation on any of the below functions
		local opts = { buffer = args.buf }
		vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
		vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
		vim.keymap.set("n", "gv", ":vsplit<CR><cmd>lua vim.lsp.buf.definition()<CR>", opts)
		vim.keymap.set("n", "S", vim.lsp.buf.hover, opts)
		vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
		vim.keymap.set("n", "<C-s>", vim.lsp.buf.signature_help, opts)
		vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, opts)
		vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, opts)
		vim.keymap.set("n", "<space>wl", function()
			print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		end, opts)
		vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, opts)
		vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)
		vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, opts)
		vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
		vim.keymap.set("n", "<space>f", function()
			vim.lsp.buf.format({ async = true })
		end, opts)
	end,
})

vim.api.nvim_create_autocmd("LspAttach", {
	group = vim.api.nvim_create_augroup("lsp_attach_disable_ruff_hover", { clear = true }),
	callback = function(args)
		local client = vim.lsp.get_client_by_id(args.data.client_id)
		if client == nil then
			return
		end
		if client.name == "ruff" then
			-- Disable hover in favor of Pyright
			client.server_capabilities.hoverProvider = false
		end
	end,
	desc = "LSP: Disable hover capability from Ruff",
})

-- python
require("lspconfig")["pyright"].setup({
	pyright = {
		-- Using Ruff's import organizer
		disableOrganizeImports = true,
	},
	python = {
		analysis = {
			-- Ignore all files for analysis to exclusively use Ruff for linting
			ignore = { "*" },
		},
	},
	capabilities = default_capabilities,
})

local ruff_config = {
	cmd = { "/run/current-system/sw/bin/ruff", "server" },
	initialization_options = {
		settings = {
			-- Ruff language server settings go here
		},
	},
}
require("lspconfig").ruff.setup(ruff_config)

require("lspconfig")["ts_ls"].setup({
	capabilities = default_capabilities,
})
require("lspconfig")["rust_analyzer"].setup({
	cmd = { "/run/current-system/sw/bin/rust-analyzer" },
	capabilities = default_capabilities,
	on_attach = function(client, bufnr)
		vim.lsp.inlay_hint.enable(bufnr)
	end,
	settings = {
		["rust-analyzer"] = {
			assist = {
				importEnforceGranularity = true,
				importPrefix = "crate",
			},
			imports = {
				granularity = {
					group = "module",
				},
				prefix = "self",
			},
			checkOnSave = {
				command = "clippy",
			},
			cargo = {
				buildScripts = {
					enable = true,
				},
			},
			procMacro = {
				enable = true,
			},
			diagnostics = {
				enable = true,
				experimental = {
					enable = true,
				},
			},
		},
	},
})

-- C++
-- clangd
local clangd_config = {
	capabilities = clangd_capabilities,
	-- cmd = { "clangd" },
	cmd = { "/run/current-system/sw/bin/clangd" },
	filetypes = { "c", "cc", "cpp", "objc", "objcpp", "cuda", "proto" },
	root_dir = root_pattern(
		".clangd",
		".clang-tidy",
		".clang-format",
		"compile_commands.json",
		"compile_flags.txt",
		"configure.ac",
		".git/",
		".vim/",
		".hg/"
	),
}
require("lspconfig")["clangd"].setup(clangd_config)

-- clangd_extensions
-- https://github.com/p00f/clangd_extensions.nvim#configuration
local clangd_extensions_config = {
	inlay_hints = {
		inline = vim.fn.has("nvim-0.10") == 1,
		-- Options other than `highlight' and `priority' only work
		-- if `inline' is disabled
		-- Only show inlay hints for the current line
		only_current_line = false,
		-- Event which triggers a refresh of the inlay hints.
		-- You can make this { "CursorMoved" } or { "CursorMoved,CursorMovedI" } but
		-- not that this may cause  higher CPU usage.
		-- This option is only respected when only_current_line and
		-- autoSetHints both are true.
		only_current_line_autocmd = { "CursorHold" },
		-- whether to show parameter hints with the inlay hints or not
		show_parameter_hints = true,
		-- prefix for parameter hints
		parameter_hints_prefix = "<- ",
		-- prefix for all the other hints (type, chaining)
		other_hints_prefix = "=> ",
		-- whether to align to the length of the longest line in the file
		max_len_align = false,
		-- padding from the left if max_len_align is true
		max_len_align_padding = 1,
		-- whether to align to the extreme right or not
		right_align = false,
		-- padding from the right if right_align is true
		right_align_padding = 7,
		-- The color of the hints
		highlight = "Comment",
		-- The highlight group priority for extmark
		priority = 100,
	},
	ast = {
		-- These are unicode, should be available in any font
		role_icons = {
			type = "🄣",
			declaration = "🄓",
			expression = "🄔",
			statement = ";",
			specifier = "🄢",
			["template argument"] = "🆃",
		},
		kind_icons = {
			Compound = "🄲",
			Recovery = "🅁",
			TranslationUnit = "🅄",
			PackExpansion = "🄿",
			TemplateTypeParm = "🅃",
			TemplateTemplateParm = "🅃",
			TemplateParamObject = "🅃",
		},
		--[[ These require codicons (https://github.com/microsoft/vscode-codicons)
            role_icons = {
                type = "",
                declaration = "",
                expression = "",
                specifier = "",
                statement = "",
                ["template argument"] = "",
            },

            kind_icons = {
                Compound = "",
                Recovery = "",
                TranslationUnit = "",
                PackExpansion = "",
                TemplateTypeParm = "",
                TemplateTemplateParm = "",
                TemplateParamObject = "",
            }, ]]

		highlights = {
			detail = "Comment",
		},
	},
	memory_usage = {
		border = "none",
	},
	symbol_info = {
		border = "none",
	},
}
require("clangd_extensions").setup(clangd_extensions_config)

-- cmake
require("lspconfig").cmake.setup({
	cmd = { "/run/current-system/sw/bin/cmake-language-server" },
	capabilities = default_capabilities,
})

-- lua_ls
local lua_ls_config = {
	capabilities = default_capabilities,
	cmd = { "/run/current-system/sw/bin/lua-language-server" },
	single_file_support = true,
	settings = {
		Lua = {
			completion = {
				callSnippet = "Replace",
			},
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = "LuaJIT",
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = { "vim" },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
		},
	},
	root_dir = root_pattern(
		".luarc.json",
		".luarc.jsonc",
		".luacheckrc",
		".stylua.toml",
		"stylua.toml",
		"selene.toml",
		"selene.yml",
		".git",
		".hg",
		"init.lua"
	),
}
require("lspconfig")["lua_ls"].setup(lua_ls_config)

-- Markdown
require("lspconfig").marksman.setup({
	cmd = { "/run/current-system/sw/bin/marksman", "server" },
	capabilities = default_capabilities,
})

-- bash
require("lspconfig").bashls.setup({
	filetypes = { "sh", "zsh" },
	capabilities = default_capabilities,
})

-- Erlang
require("lspconfig").erlangls.setup({
	capabilities = default_capabilities,
})

-- Nix
require("lspconfig").nil_ls.setup({
	cmd = { "/run/current-system/sw/bin/nil" },
	capabilities = default_capabilities,
})

-- Eslint
require("lspconfig").eslint.setup({
	capabilities = default_capabilities,
	on_attach = function(client, bufnr)
		vim.api.nvim_create_autocmd("BufWritePre", {
			buffer = bufnr,
			command = "EslintFixAll",
		})
	end,
})

-- devicons
require("nvim-web-devicons").setup({})

-- null-ls
local null_ls = require("null-ls")
local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
local null_ls_config = {
	sources = {
		null_ls.builtins.formatting.stylua,
		null_ls.builtins.formatting.prettier,
		null_ls.builtins.formatting.black,
		null_ls.builtins.formatting.shfmt.with({
			filetypes = { "sh", "zsh" },
		}),
		null_ls.builtins.formatting.clang_format.with({
			filetypes = { "c", "cpp", "cc", "cs", "cuda" },
		}),
		null_ls.builtins.formatting.cmake_format,
		-- null_ls.builtins.formatting.rustfmt,
		null_ls.builtins.formatting.erlfmt,
		null_ls.builtins.formatting.nixfmt,
		-- null_ls.builtins.diagnostics.eslint,
		null_ls.builtins.diagnostics.pylint,
		null_ls.builtins.diagnostics.zsh,
		null_ls.builtins.diagnostics.todo_comments,
		null_ls.builtins.completion.spell,
		null_ls.builtins.completion.luasnip,
		null_ls.builtins.code_actions.statix, -- Nix
		null_ls.builtins.code_actions.gitsigns,
	},
	on_attach = on_attach,
}
null_ls.setup(null_ls_config)

-- General keymap
vim.keymap.set("i", "<C-j>", "<Down>")
vim.keymap.set("i", "<C-h>", "<Left>")
vim.keymap.set("i", "<C-k>", "<Up>")
vim.keymap.set("i", "<C-l>", "<Right>")
vim.keymap.set("n", "<CR>", "o<Esc>k") -- newline below without insert mode
vim.keymap.set("n", "<S-Enter>", "O<Esc>j") -- newline above without insert mode
vim.keymap.set("i", "(<CR>", "(<CR>)<C-o>O")
vim.keymap.set("i", "[<CR>", "[<CR>]<C-o>O")
vim.keymap.set("i", "{<CR>", "{<CR>}<C-o>O")
-- vim.keymap.set("i", '"', '""<Esc>i')
-- vim.keymap.set("i", "{", "{}<Esc>i")
-- vim.keymap.set("i", "[", "[]<Esc>i")
-- vim.keymap.set("i", "(", "()<Esc>i")
vim.keymap.set("i", "(", "()<Left>")
vim.keymap.set("i", "[", "[]<Left>")
vim.keymap.set("i", "{", "{}<Left>")
vim.keymap.set("i", '"', '""<Left>')

-- Global vim settings
vim.opt.expandtab = true
vim.opt.showmatch = true
vim.opt.incsearch = true
vim.opt.hlsearch = true -- highlight all matches on previous search pattern
vim.opt.formatoptions = "croqlt12"
vim.opt.textwidth = 0
vim.opt.wrapmargin = 0
vim.opt.backspace = "indent,eol,start"
vim.opt.shortmess = "filnxtToOFc" -- don't give ins-completion-menu messages
vim.opt.termguicolors = true -- set termguicolors to enable highlight groups
vim.opt.guicursor = "i-ci:block-blinkon2"
vim.opt.backup = false -- creates a backup file
vim.opt.clipboard = "unnamedplus" -- allows neovim to access the system clipboard
vim.opt.cmdheight = 2 -- more space in the neovim command line for displaying messages
vim.opt.colorcolumn = "80" -- fixes indentline for now
vim.opt.completeopt = { "menuone", "noselect" }
vim.opt.conceallevel = 0 -- so that `` is visible in markdown files
vim.opt.fileencoding = "utf-8" -- the encoding written to a file
vim.opt.encoding = "utf-8"
vim.opt.foldmethod = "indent" -- folding set to "expr" for treesitter based folding
vim.opt.foldlevelstart = 99
vim.opt.foldnestmax = 10
vim.opt.foldexpr = "" -- set to "nvim_treesitter#foldexpr()" for treesitter based folding
vim.opt.guifont = "monospace:h17" -- the font used in graphical neovim applications
vim.opt.hidden = true -- required to keep multiple buffers and open multiple buffers
vim.opt.ignorecase = true -- ignore case in search patterns
vim.opt.mouse = "a" -- allow the mouse to be used in neovim
vim.opt.pumheight = 10 -- pop up menu height
vim.opt.showmode = false -- we don't need to see things like -- INSERT -- anymore
vim.opt.showtabline = 2 -- always show tabs
vim.opt.smartcase = true -- smart case
vim.opt.smartindent = true -- make indenting smarter again
vim.opt.splitbelow = true -- force all horizontal splits to go below current window
vim.opt.splitright = true -- force all vertical splits to go to the right of current window
vim.opt.swapfile = false -- creates a swapfile
vim.opt.termguicolors = true -- set term gui colors (most terminals support this)
vim.opt.timeout = true
vim.opt.ttimeoutlen = 100
vim.opt.timeoutlen = 3000
vim.opt.title = true -- set the title of window to the value of the titlestring
vim.opt.titlestring = "%<%F%=%l/%L - nvim" -- what the title of the window will be set to
vim.opt.undodir = vim.fn.stdpath("cache") .. "/undo"
vim.opt.undofile = true -- enable persistent undo
vim.opt.updatetime = 250 -- faster completion
vim.opt.writebackup = false -- if a file is being edited by another program (or was written to file while editing with another program) it is not allowed to be edited
vim.opt.shiftwidth = 2 -- the number of spaces inserted for each indentation
vim.opt.tabstop = 4 -- insert 4 spaces for a tab
vim.opt.smarttab = true
vim.opt.softtabstop = 4
vim.opt.cursorline = true -- highlight the current line
vim.opt.number = true -- set numbered lines
vim.opt.relativenumber = false -- set relative numbered lines
vim.opt.numberwidth = 4 -- set number column width to 2 {default 4}
vim.opt.signcolumn = "yes" -- always show the sign column otherwise it would shift the text each time
vim.opt.wrap = true -- wrap long lines (softly)
vim.opt.linebreak = true
vim.opt.spell = true
vim.opt.spelllang = "en"
vim.opt.scrolloff = 8 -- is one of my fav
vim.opt.sidescrolloff = 8
vim.opt.list = true
vim.opt.listchars:append({ space = "⋅" })
vim.opt.listchars:append({ eol = "↴" })
