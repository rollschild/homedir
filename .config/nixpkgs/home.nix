{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "rollschild";
  home.homeDirectory = "/home/rollschild";

  nixpkgs.config.allowUnfree = true;

  home.packages = with pkgs; [
    firefox
    swaylock
    swayidle
    wl-clipboard
    mako # notification daemon
    alacritty # Alacritty is the default terminal in the config
    dmenu # Dmenu is the default in the config but i recommend wofi since its wayland native
    konsole
    wl-clipboard
    brightnessctl
    vlc
    fzf
    ripgrep
    direnv
    ctags
    gtk-engine-murrine
    gtk_engines
    gsettings-desktop-schemas
    lxappearance
    xclip
    dropbox-cli
    nodejs
    nodePackages.typescript
    nodePackages.prettier
    nodePackages.npm
  ];

  # Overlays
  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/neovim-nightly-overlay/archive/master.tar.gz;
    }))
  ];

  # NeoVim
  programs.neovim = {
    enable = true;
    package = pkgs.neovim-nightly;
    withPython3 = true;
    withNodeJs = true;
    extraPython3Packages = (ps: with ps; [ 
      jedi 
      flake8 
      pep8 
      python-language-server
    ]);
    plugins = with pkgs.vimPlugins; [
      gruvbox
      jellybeans-vim
      vim-go
      vim-fugitive
      coc-nvim
      vim-tmux-navigator
      vim-tmux
      vimux
      fzf-vim
      coc-fzf
      ack-vim
      indentLine
      nerdcommenter
      vim-sensible
      vim-sleuth
      vim-gitgutter
      emmet-vim
      ale
      lightline-vim
      vim-javascript
      vim-jsx-pretty
      typescript-vim
      tsuquyomi
      vimproc-vim
      vim-misc
      vim-lsp
      syntastic
      vim-autoformat
      direnv-vim
      nerdtree
      nerdtree-git-plugin
      tabular
      vim-markdown
      coc-prettier
      coc-css
      coc-emmet
      coc-eslint
      coc-explorer
      coc-git
      coc-highlight
      coc-html
      coc-jest
      coc-json
      coc-lists
      coc-markdownlint
      coc-python
      coc-snippets
      coc-stylelint
      coc-tsserver
      coc-yaml
      coc-yank
      nvim-treesitter
    ];
    extraConfig = ''
      set hidden
      set colorcolumn=80 
      set mouse=a
      set cmdheight=2
      set nobackup
      set nowritebackup
      " don't give |ins-completion-menu| messages.
      set shortmess+=c
      set clipboard=unnamedplus
      " always show signcolumns
      set signcolumn=yes
      set number
      set updatetime=100
      set timeout timeoutlen=3000 ttimeoutlen=100
      set tabstop=4
      set smarttab
      set softtabstop=4
      set expandtab
      set textwidth=79
      set colorcolumn=80
      set wrapmargin=0
      set formatoptions=croqlt12
      set wrap
      set shiftwidth=2
      set ignorecase
      set noswapfile
      set autoindent
      set smartindent
      set showcmd
      set cursorline
      set guicursor+=i:block-Cursor
      set wildmenu
      set lazyredraw
      set showmatch
      set incsearch
      set hlsearch
      set foldenable
      set cindent
      set shell=/run/current-system/sw/bin/bash
      set viminfo='100,<1000,s100,h
      colorscheme gruvbox
      set background=dark
      set termguicolors
      let g:gruvbox_contrast_dark='hard'
      let g:gruvbox_improved_strings='1'
      set foldlevelstart=99
      set foldnestmax=10
      nnoremap <space> za
      set foldmethod=indent
      inoremap ( ()<Esc>i
      inoremap (<CR> (<CR>)<C-o>O
      inoremap [ []<Esc>i
      inoremap [<CR> [<CR>]<C-o>O
      "inoremap < <><Esc>i
      inoremap { {}<Esc>i
      inoremap {<CR> {<CR>}<C-o>O
      "inoremap <C-Return> <CR><CR><C-o>k<Tab>
      inoremap " ""<Esc>i
      inoremap ` ``<Esc>i
      inoremap `<CR> `<CR>`<C-o>O

      " new line without insert mode
      nmap <S-Enter> O<Esc>j
      nmap <CR> o<Esc>k

      " navigation
      nnoremap <C-J> <C-W><C-J>
      nnoremap <C-K> <C-W><C-K>
      nnoremap <C-L> <C-W><C-L>
      nnoremap <C-H> <C-W><C-H>
      set splitbelow
      set splitright

      " Use tab for trigger completion with characters ahead and navigate.
      " Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
      " use <tab> for trigger completion and navigate to the next complete item
      function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~ '\s'
      endfunction

      inoremap <silent><expr> <TAB>
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ coc#refresh()
      inoremap <expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
      inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<S-TAB>"

      " jump to definition file
      nnoremap <silent> gf :call CocAction('jumpDefinition')<CR>

      " fugitive
      autocmd QuickFixCmdPost *grep* cwindow
      " https://github.com/thoughtbot/dotfiles/issues/655#issuecomment-605019271
      if &diff
          set diffopt-=internal
          set diffopt+=vertical
      endif

      " Vimux
      " Prompt for a command to run
      map <Leader>vp :VimuxPromptCommand<CR>
      " Run last command executed by VimuxRunCommand
      map <Leader>vl :VimuxRunLastCommand<CR>

      " Coc config
      set completeopt+=preview
      set completeopt+=menuone
      set completeopt+=noselect
      autocmd CursorHold * silent call CocActionAsync('highlight')

      autocmd FileType json syntax match Comment +\/\/.\+$+
      let g:ale_linters = {
          \ 'sh': ['language_server'],
          \ }

      autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

      " gitgutter
      let g:gitgutter_max_signs = 10000

      " NERD Commenter
      " Add spaces after comment delimiters by default
      let g:NERDSpaceDelims = 1
      " Use compact syntax for prettified multi-line comments
      let g:NERDCompactSexyComs = 1
      " Align line-wise comment delimiters flush left instead of following code indentation
      let g:NERDDefaultAlign = 'left'
      " Set a language to use its alternate delimiters by default
      let g:NERDAltDelims_java = 1
      " Add your own custom formats or override the defaults
      let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
      " Allow commenting and inverting empty lines (useful when commenting a region)
      let g:NERDCommentEmptyLines = 1
      " Enable trimming of trailing whitespace when uncommenting
      let g:NERDTrimTrailingWhitespace = 1
      " Enable NERDCommenterToggle to check all selected lines is commented or not 
      let g:NERDToggleCheckAllLines = 1

      " coc-jest
      " Run jest for current project
      command! -nargs=0 Jest :call  CocAction('runCommand', 'jest.projectTest')

      " Run jest for current file
      command! -nargs=0 JestCurrent :call  CocAction('runCommand', 'jest.fileTest', ['%'])

      " Run jest for current test
      nnoremap <leader>te :call CocAction('runCommand', 'jest.singleTest')<CR>

      " Init jest in current cwd, require global jest command exists
      command! JestInit :call CocAction('runCommand', 'jest.init')

      let g:lightline = {
            \ 'colorscheme': 'one',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ],
            \             [ 'cocstatus', 'currentfunction', 'readonly', 'filename', 'modified' ] ]
            \ },
            \ 'component_function': {
            \   'cocstatus': 'coc#status',
            \   'currentfunction': 'CocCurrentFunction'
            \ },
            \ }

      function! CocCurrentFunction()
          return get(b:, 'coc_current_function', "")
      endfunction

      " Use <c-space> to trigger completion.
      if has('nvim')
        inoremap <silent><expr> <c-space> coc#refresh()
      else
        inoremap <silent><expr> <c-@> coc#refresh()
      endif

      " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
      " position. Coc only does snippet and additional edit on confirm.
      " <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
      if exists('*complete_info')
        inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
      else
        inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
      endif

      augroup mygroup
        autocmd!
        " Setup formatexpr specified filetype(s).
        autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
        " Update signature help on jump placeholder
        autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
      augroup end

      " Use K to show documentation in preview window
      nnoremap <silent> K :call <SID>show_documentation()<CR>

      function! s:show_documentation()
        if (index(['vim','help'], &filetype) >= 0)
          execute 'h '.expand('<cword>')
        else
          call CocAction('doHover')
        endif
      endfunction

      " fzf search
      " Default fzf layout
      " - down / up / left / right
      let g:fzf_layout = { 'down': '~30%' }

      let g:fzf_action = {
        \ 'ctrl-t': 'tab split',
        \ 'ctrl-i': 'split',
        \ 'ctrl-s': 'vsplit' }

      autocmd!  FileType fzf set laststatus=0 noshowmode noruler
        \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler

      command! -bang -nargs=* Rg
        \ call fzf#vim#grep(
        \   'rg --hidden --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
        \   <bang>0 ? fzf#vim#with_preview('up:60%')
        \           : fzf#vim#with_preview('right:50%:hidden', '?'),
        \   <bang>0)

      nnoremap <C-g> :Rg<Cr>

      " Global line completion (not just open buffers. ripgrep required.)
      inoremap <expr> <c-x><c-l> fzf#vim#complete(fzf#wrap({
        \ 'prefix': '^.*$',
        \ 'source': 'rg -n ^ --color always',
        \ 'options': '--ansi --delimiter : --nth 3..',
        \ 'reducer': { lines -> join(split(lines[0], ':\zs')[2:], "") }}))

      " [Buffers] Jump to the existing window if possible
      " let g:fzf_buffers_jump = 1

      nnoremap <silent> <Leader>s :call fzf#run({
      \   'down': '40%',
      \   'sink': 'botright split' })<CR>

      " Open files in vertical horizontal split
      nnoremap <silent> <Leader>v :call fzf#run({
      \   'right': winwidth('.') / 2,
      \   'sink':  'vertical botright split' })<CR>

      let g:ackprg = 'rg --vimgrep --no-heading'
      cnoreabbrev ag Ack
      cnoreabbrev aG Ack
      cnoreabbrev Ag Ack
      cnoreabbrev AG Ack

      let g:vim_jsx_pretty_colorful_config = 1
      let g:typescript_indent_disable = 0
      let g:typescript_ignore_browserwords = 0

      set statusline+=%#warningmsg#
      set statusline+=%{SyntasticStatuslineFlag()}
      set statusline+=%{coc#status()}
      set statusline+=%*

      let g:syntastic_python_checkers = ['pylint']
      let g:syntastic_always_populate_loc_list = 1
      let g:syntastic_auto_loc_list = 1
      let g:syntastic_check_on_open = 1
      let g:syntastic_check_on_wq = 1
      let g:syntastic_javascript_checkers = ['eslint']
      let g:syntastic_javascript_eslint_exe = 'npm run lint --'
      let g:elm_syntastic_show_warnings = 1

      " coc-yank
      nnoremap <silent> <space>y  :<C-u>CocList -A --normal yank<cr>

      " coc-explorer
      nnoremap <space>e :CocCommand explorer<CR>

      " NERDTree
      let g:NERDTreeWinSize = 32
      let NERDTreeCustomOpenArgs = {'file': {'reuse': 'all', 'where': 'p', 'keepopen': 1}, 'dir': {}}
      " autocmd vimenter * NERDTree
      autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
      map <C-n> :NERDTreeToggle<CR>

      " start vim/nvim with $vim or $nvim, NOT $vim . or $nvim .
      autocmd StdinReadPre * let s:std_in=1
      autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

      autocmd StdinReadPre * let s:std_in=1
      autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

      let g:NERDTreeGitStatusUseNerdFonts = 1
      " Please customize this:
      let g:NERDTreeGitStatusIndicatorMapCustom = {
          \ "Modified"  : "✹",
          \ "Staged"    : "✚",
          \ "Untracked" : "✭",
          \ "Renamed"   : "➜",
          \ "Unmerged"  : "═",
          \ "Deleted"   : "✖",
          \ "Dirty"     : "✗",
          \ "Clean"     : "✔︎",
          \ 'Ignored'   : '☒',
          \ "Unknown"   : "?"
          \ }

      let g:prettier#config#print_width = 80
      let g:prettier#config#html_whitespace_sensitivity = 'css'
      let g:prettier#config#trailing_comma = 'all'
      let g:prettier#config#semi = 'true'
      let g:prettier#autoformat = 1
      let g:prettier#autoformat_require_pragma = 0
      let g:prettier#exec_cmd_async = 1
      let g:prettier#config#tab_width = 2
      " css|strict|ignore
      " default: 'css'
      let g:prettier#config#html_whitespace_sensitivity = 'css'

      command! -nargs=0 Prettier :CocCommand prettier.formatFile

      " Auto-format Go
      autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')
    '';
  };

  # tmux
  programs.tmux = {
    enable = true;
    clock24 = true;
    extraConfig = ''
      # change global prefix
      set-option -g prefix C-a
      set -g mouse on

      # increase scrollback lines
      set -g history-limit 10000

      set-option -sg escape-time 10

      # Enable 24 bit true colors
      set -ga terminal-overrides ',*:Tc'
      set -g default-terminal "tmux-256color"
      set-option -sa terminal-overrides ',xterm-256color:RGB'

      #set inactive/active window styles
      set -g window-style 'fg=colour247,bg=colour236'
      set -g window-active-style 'fg=default,bg=colour234'

      set-option -s set-clipboard off
      bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"
      bind-key -T copy-mode MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'reattach-to-user-namespace pbcopy'
      is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
              | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
      is_fzf="ps -o state= -o comm= -t '#{pane_tty}' \
              | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?fzf$'"
      bind -n C-h run "($is_vim && tmux send-keys C-h) || \
                          tmux select-pane -L"
      bind -n C-j run "($is_vim && tmux send-keys C-j)  || \
                               ($is_fzf && tmux send-keys C-j) || \
                               tmux select-pane -D"
      bind -n C-k run "($is_vim && tmux send-keys C-k) || \
                                ($is_fzf && tmux send-keys C-k)  || \
                                tmux select-pane -U"
      bind -n C-l run  "($is_vim && tmux send-keys C-l) || \
                                tmux select-pane -R"

      bind | split-window -h -c "#{pane_current_path}"
      bind - split-window -v -c "#{pane_current_path}"
    '';
  };

  # fonts
  fonts = {
    fontconfig = {
      enable = true;
    };
  };
  gtk.font = {
    package = pkgs.inconsolata;
    name = "Inconsolata";
    size = "11";
  };

  # Sway
  wayland.windowManager.sway = {
    enable = true;
    wrapperFeatures.gtk = true ;
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.05";
}
